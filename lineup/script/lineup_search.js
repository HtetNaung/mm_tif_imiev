
/* carlineup search v0.95*/
var comp = new Array();

var popuptxt = new Array();
var one = true;
var focusflag = false;
var focusclick = 0;
var ret;
var befheight;

ukk = {};

ukk.$ = function(IDS){
	if(ukk.stringChk(IDS)) return document.getElementById(IDS);
	else return null;
}
ukk.stringChk = function(object){
	if(typeof object == 'string' || object instanceof String) return object;
	return null;
}

ukk.getElementsByTagNameArray = function(tagname,element){
	if(typeof element == 'undefined') var doc = document;
	else var doc = element;
	
	var tagAll = [];
	if(!ukk.stringChk(tagname)) return;
	var getname = doc.getElementsByTagName(tagname);
	var len = getname.length;
	
	while(len--) tagAll[tagAll.length] = getname[tagAll.length];
	
	return tagAll;
}
ukk.$$ = function(CLS,element){
	
	if(!ukk.stringChk(CLS)) return;
	
	var tagAll = ukk.getElementsByTagNameArray('*',element);
	var len = tagAll.length;
	var cl5 =[];
	
	for (i=0;i<len;i++){
		var classes = tagAll[i].getAttribute('class') || tagAll[i].getAttribute('className');
		if(classes){
			var classnm = classes.split(' ');
			var clsArray = CLS.split(' ');
			var clsstay;
			var clearflg = 0;
			for(var z=0;z<classnm.length;z++){
			
				if(CLS.indexOf(' ') != -1 && (CLS.lastIndexOf(' ') != CLS.length-1)){			
				for(var k=0;k<clsArray.length;k++){
				
					if(clsArray[k] == classnm[z]){				
						clearflg++;
						break;
					}
				}
				if(clearflg == clsArray.length){
						cl5[cl5.length] = tagAll[i];
				}
				}else{
				
				if(CLS.lastIndexOf(' ') == CLS.length-1){
					CLS = CLS.substring(0,CLS.length-1);
				}
				
				if (CLS == classnm[z]){
					cl5[cl5.length] = tagAll[i];
				}
				}
			}
		}
	}
	
	return cl5
}

/* ブラウザ判別 */
ukk.UA = (function(){
	var doc = document;
	var ua = navigator.userAgent.toUpperCase();
	var apnm = navigator.appName.toUpperCase();
	var apver = navigator.appVersion;

	var barray = [];
	
	var mac = ua.indexOf("MAC",0) >= 0;
	var windows = ua.indexOf("WIN",0) >= 0;
	
	if(windows){
		barray['os'] = 'windows';
	}else if(mac){
		barray['os'] = 'mac';
	}else{
		barray['os'] = 'other'
	}
	
	
	if(window.attachEvent && !window.opera){
		barray['name'] = 'IE';
		
		if (typeof doc.documentElement.style.msInterpolationMode != 'undefined') {
			if(typeof document.documentMode != 'undefined') {
			barray['ver'] = '8';
			}else{
			barray['ver'] = '7';
			}
		}else{
			if(ua.indexOf('MSIE 6',0) >= 0){
				barray['ver'] = '6';
			}
		}
	}else{	

	if(ua.indexOf("SAFARI",0) >= 0 && ua.indexOf('APPLEWEBKIT/',0) >= 0){
		
			barray['name'] = 'Safari';
		
			barray['ver'] = (function(){
				var ver = ua.split("/");
				var vernum = ver[ver.length-1];
				vernum = vernum.replace(/\./g,'');
				vernum = vernum.slice(0,3);
				var n = parseInt(vernum,10)
				if (n >= 412 && n < 522){
				return '2';
				}else if (n >= 522) {
				return '3';
				}else{
				return '1';
				}
				})();
	}else if(ua.indexOf('GECKO',0) >= 0 && ua.indexOf('FIREFOX',0) >= 0){
		
		barray['name'] = 'Firefox';
		
		var ffua = ua.split('/');
		var ffver = ffua[ffua.length-1].slice(0,1);
		
		if (typeof window.postMessage != 'undefined' && ffver == '3') {
			barray['ver'] = '3';		
		}else if(ffver == '2'){
			barray['ver'] = '2';
		}else{
			barray['ver'] = '1';
		}
		
	}else if(window.opera){
		barray['name'] = 'Opera';
	}else if(ua.indexOf('NETSCAPE',0) >= 0){
		barray['name'] = 'Netscape';
		
		if(apver == '5'){
			barray['ver'] = '6';
		}
	}else{
		barray['name'] = 'unknown';
	}
	
	}
	
	return barray;
})();


ukk.Browser = function(target,version){	
	if(typeof target == 'undefined') return null;
	if(!ukk.stringChk(target)) return null;
	
	if(typeof version == 'undefined') var ver = false;
	else var ver = true; 
	
	var nm = ukk.UA.name.toUpperCase();
	var target = target.toUpperCase();
	
	if(nm.search(target) != -1) {
		
		if(ver){
			
			if(ukk.UA.ver.search(version,'i') != -1){
			return true;
			}else{
			return false;
			}		
		}
		return true;	
	}else{
		return false;
	}
}




var carlineupClass = Class.create({
	
	initialize : function(viewtype) {
	
	this.viewtype = viewtype;
	this.selectTag = [];
	this.selectdef = [];
	this.selectnum = [];
	
	this.optelems = [];
	this.optListelems = [];
	
	this.visibleCar = [];
	
	this.selectStatus = 0;

	this.checkflg = [];
	this.optionbox = [];
	this.imgcount = 0;
	this.effg = true;
	
	},
  
	selectEventSet : function (){
	
	$(this.viewtype + 'resetbt').observe('click',this.formReset.bind(this));
	
	for(var g=0;g<this.selectTag.length;g++){
	Event.observe(this.selectTag[g],'change',this.selectEv.bind(this));
	if(winIE){
	Event.observe(this.selectTag[g],'focus',this.selectEvclick);
	Event.observe(this.selectTag[g],'click',this.selectEvclick);
	Event.observe(this.selectTag[g],'mouseover',this.selectEvdown);
	Event.observe(this.selectTag[g],'mouseout',this.selectEvup.bind(this));
	Event.observe(this.selectTag[g],'blur',this.selectEvblur.bind(this));
	}
	}
	
	var pre = this;
	this.optelems.each(function(obj){
		Event.observe(obj,'click', pre.optSelect.bind(pre));
		if(pre.viewtype == 'carlineup'){
		Event.observe(obj,'mouseover', pre.optOver);
		Event.observe(obj,'mouseout', pre.optOut);
		}
	});
	
	this.optListelems.each(function(obj){
		if(pre.viewtype == 'carlineup'){
		Event.observe(obj,'mouseover', pre.optOver);
		Event.observe(obj,'mouseout', pre.optOut);
		}
	});
	
	},
	
	optionSet : function(){
		
	var cbox = [];

	for(var g=0;g<this.selectdef.length;g++){
	cbox[g] = [];
	this.selectTag[g].disabled = true;
			
	//プルダウン初期化
	for(var j=0;j<this.selectdef[g].length;j++){
		//プルダウン表示フラグ初期化
		if(j == 0) cbox[g][0] = true;
		else cbox[g][j] = false;
	}
	}
	
	if(this.viewtype != 'hearty'){
	var cobox = [];
	//チェックボックス初期化
	for(var f=0;f<this.option.length;f++){
		cobox[f] = true;
	}
	}
	var len=this.visibleCar.length;
	for(var z=0;z<len; z++){

	var selectbox = ukk.getElementsByTagNameArray('select',this.carElm[this.visibleCar[z]]);

	//var selectbox = this.carElm[this.visibleCar[z]].getElementsByTagName('select');	
	var searchselect = [];
		
	for(var r=0;r<selectbox.length;r++){
		searchselect[r] = selectbox[r].getAttribute('value');
		if(searchselect[r].length > 1){
			var arrayboxB= searchselect[r].split(',');
			for(var x=0;x<arrayboxB.length;x++){
				cbox[r][arrayboxB[x]] = true;
			}
		}else{
			cbox[r][0] = true;
		}
	}
	
	if(this.viewtype != 'hearty'){
	var optCar = this.carElm[this.visibleCar[z]].getElementsByTagName('option')[0].getAttribute('value');
		if(optCar.length > 1){
			var arrayboxO= optCar.split(',');
			for(var x=0;x<arrayboxO.length;x++){
				cobox[arrayboxO[x]] = false;
			}
		}else{
			cobox[optCar] = false;
		}		
	}
	}
	
	//対象外のプルダウン削除
	
	for(var g=0,len=this.selectTag.length;g<len;g++){
		
	//プルダウン初期化
	var slnm = this.selectdef[g].length;
	
	for(var j=0;j<slnm;j++){
		this.selectTag[g][j] = new Option(this.selectdef[g][j],j);
		this.selectTag[g][j].selected = false;
	}
	
	for(var f=slnm-1;f>0;f--){
		
		if(!cbox[g][f] && cbox[g].length > 2){
			Element.remove(this.selectTag[g][f]);
		}else if(this.selectStatus == (g+1)){
			this[this.viewtype + "selctbox"+(g+1)] = this.selectnum[g];
			if(this.selectTag[g][f].value == this.selectnum[g])	this.selectTag[g][this.selectnum[g]].selected = true;
		}else if(this.selectTag[g][f].value == this[this.viewtype + "selctbox"+(g+1)]){
			this.selectTag[g][f].selected = true;
		}
		
	}
	this.selectTag[g].disabled = false;
	}
	
	if(this.viewtype != 'hearty'){
	/*オプション装備*/
	for(var j=0;j<this.option.length;j++){
		if(this.viewtype == 'carlineup'){
		Event.stopObserving(this.optionList[j]);
		}
		this.option[j].disabled = true;	
		Element.addClassName(this.optionList[j], 'imgoff');
		
		if(cobox[j] == false){
			
		this.option[j].disabled = cobox[j];
		Element.removeClassName(this.optionList[j], 'imgoff');
		if(this.viewtype == 'carlineup'){
		Event.observe(this.optionList[j],'mouseover', this.optOver);
		Event.observe(this.optionList[j],'mouseout', this.optOut);
		}
		}
	}
	}
	},
	
	optOver : function(event){
	event.stop();
	var tg = Event.element(event);
	var tgelm = ukk.$('tip' + tg.parentNode.id);
	
	Element.setStyle(tgelm, {opacity:'0'});

	var offset = Position.cumulativeOffset(this.parentNode);
	var dimen = Element.getDimensions(this.parentNode);

	tgelm.style.top = offset[1]-Element.getHeight(tgelm)-2;
	tgelm.style.left = dimen.width-20;
	tgelm.style.display = 'block';
	
	new Effect.Opacity(tgelm, { from:0.0, to:1.0,duration :0.3, afterFinishInternal: function(effect) { 
	Element.removeClassName(tgelm,'alpha0');
 	},limit:1
	});
	
	this.style.cursor = "pointer";
	
	},
	
	optOut : function(event){
	event.stop();
	var tg = Event.element(event);
	var tgelm = ukk.$('tip' + tg.parentNode.id);
	
	tgelm.style.display = 'none';
	this.style.cursor = "auto";
	},
	
	tipnone : function(event){
	this.style.display = 'none';
	//Element.addClassName(this,'alpha0');
	},
	
 
	selectEv : function(event){
		
	var elemnum = Event.element(event).id.split("_")[1];
	this.selectStatus = Number(elemnum);
	this.selectObj = Event.element(event);
	this.selectmain('select');
	},
	
	selectEvdown : function(event){
			
	if(this.id == 'heartyselect_2') {
	
	ukk.$(thisHash + 'ConditionBlock').style.width = 'auto';
	this.style.width = 'auto';	
	this.style.zIndex = 999;
		
	}
	
	},

	selectEvclick : function(){
	if(this.id == 'heartyselect_2') {	
	focusflag = true;
	}
	},

	selectEvblur : function(){
	if(!focusflag) return;
		focusclick = 0;
		focusflag = false;
		ukk.$('heartyselect_2').style.width = 140 +'px';
		ukk.$(thisHash + 'ConditionBlock').style.width = 171 + 'px';
	},
	
	selectEvup : function(event){
	
	if(focusflag) return; 
	var selectObj = Event.element(event);
	
	if(selectObj.id == 'heartyselect_2') {
		selectObj.style.width = 140 +'px';
		ukk.$(thisHash + 'ConditionBlock').style.width = 171 + 'px';
	//	selectObj.style.zIndex = 999;
	}
	
	},
	
	optSelect : function(){
		
	if(this.effg)


	this.checkflg = [];
	//this.checkflg[0] = 0;
	
	for(var b=0;b<this.option.length;b++){
		if(this.option[b].checked == true){
			this.checkflg[this.checkflg.length] = this.option[b].id.match(/\d/)-1;
		}
	}
	this.selectmain('opt');
	},
	
	
	/* 絞り込み処理ここから ------------------------------------------------------------ ------------------------------  */
	
	selectmain : function(evtype){		

	if(evtype == 'select'){
	this.selectObj.blur();
	this.selectObj[this.selectObj.selectedIndex].selected =true;
	}
	
	//this.imageReset();
	var pre = this;
	
	for(var g=0;g<this.selectTag.length;g++){
	for(var t=0;t<this.selectTag[g].length;t++){
	if(this.selectTag[g][t].selected == true) this.selectnum[g] = this.selectTag[g][t].value;
	//if(this.selectTag[g][t].selected == true) 
	}
	}
		
	this.visibleCar = [];
	
	/**/

	var optionar =[];
	this.num = 0;
	this.effg = true;
	var carnum = this.carElm.length;
	
	for(var i=0;i<carnum;i++){
		
		var selectbox = ukk.getElementsByTagNameArray('select',this.carElm[i]);

		//var selectbox = this.carElm[i].getElementsByTagName('select');
		var searchselect = [];
		var clearfg = 0;
		var slnm = selectbox.length;
		
		for(var r = 0,len = slnm;r<len;r++){
			searchselect[r] = selectbox[r].getAttribute('value');
		}
		
		var opbox = this.carElm[i].getElementsByTagName('option')[0];
	
		var carid = ukk.$(this.viewtype + 'car' + i);
				
		for(var k=0;k<slnm;++k){
			
			if(searchselect[k].indexOf(this.selectnum[k]) != -1) {
				clearfg++;
				if(clearfg == slnm){
					if(this.checkflg != ''){
						
						clearfg = 0;
						
						for(var g=0;g<this.checkflg.length;g++){
							if(opbox.getAttribute('value').indexOf(this.checkflg[g]) != -1) {
								clearfg++;
								if(clearfg == this.checkflg.length){
								this.visibleControl('visible',carid);
								this.visibleCarArray(i);
								}
							}else{
								this.visibleControl('hidden',carid);
							}
						}
					}else{
						this.visibleControl('visible',carid);
						this.visibleCarArray(i);
					}
				}
			}else{
				this.visibleControl('hidden',carid);
			}
		}	
	}
	this.optionSet();
	},
	
	/* ------------------------------------------------------------ ------------------------------------------------------------ */
	
	formReset : function(){ //フォームリセット
	
	if(!this.effg) return;
	
	Form.reset(this.viewtype + 'Search');
	this.imageReset("reset");
	
	this.selectnum = new Array();
	this.checkflg.clear();
	this.visibleCar = new Array();
	
	this.selectStatus = 0;
	
	this.checkflg = new Array();
	this.optionbox = new Array();
		
	//プルダウン初期化
		
	var cbox = new Array();
	
	for(var g=0;g<this.selectdef.length;g++){

	cbox[g] = new Array();
	this.selectTag[g].disabled = false;
			
	//プルダウン初期化
	for(var j=0;j<this.selectdef[g].length;j++){
		this.selectTag[g][j] = new Option(this.selectdef[g][j],j);	
		this.selectTag[g][j].selected = false;
		
		//プルダウン表示フラグ初期化
		if(j == 0) cbox[g][0] = true;
		else cbox[g][j] = false;
 
	}
	this[this.viewtype + "selctbox"+(g+1)] = 0;
	}
	
	
	this.optelems.each(function(obj){
	obj.disabled = false;	
	});
	var pre = this;
	this.optListelems.each(function(obj){
		if(pre.viewtype == 'carlineup'){
		Event.stopObserving(obj);
		Event.observe(obj,'mouseover', pre.optOver);
		Event.observe(obj,'mouseout', pre.optOut);
		}
		Element.removeClassName(obj, 'imgoff');							  
	});
	

	},
	
	imageReset : function(check){
	for(var i=0;i<this.carElm.length;i++){
		var thisid = $(this.viewtype + 'car' + i);
		this.visibleControl('visible',thisid);
	}
	},
	
	visibleControl : function(visibility,elms){
	var pre = this;
	//var elms = ukk.$(elm);
	var imgelm = elms.getElementsByTagName('img');
	if(imgelm.length > 1) var imgelm1 = imgelm[1];
	else var imgelm1 = imgelm[0];
	
	var hiddenName = elms.getElementsByTagName('p')[1];	
	
	if(visibility == 'visible' && Element.hasClassName(elms, 'imgoff'))	{
	
	hiddenName.style.visibility = 'visible';

	if(!ukk.Browser('IE',6) && !ukk.Browser('Firefox',2) && !ukk.Browser('Safari',2)){
	Element.setStyle(elms,{opacity:'0.3'});
	new Effect.Opacity(elms, { from:0.3, to:1.0,duration :0.4});
	
	}
	
	Element.removeClassName(elms, 'imgoff');
	
	if(elms.getElementsByTagName("a")[0]){
	elms.observe('mouseover',boxovert);
	elms.observe('mouseout',boxoverf);
	Event.observe(imgelm1,'click',boxclick);
	Event.observe(imgelm1,'mouseover',imgovert);
	Event.observe(imgelm1,'mouseout',imgoverf);
	}
	
	if(this.visibleCar.length == this.carElm.length) this.effg = false;
	
	}else if(visibility == 'hidden' && !Element.hasClassName(elms, 'imgoff')){
	
	Element.addClassName(elms, 'imgoff');
   	
	elms.stopObserving();
	Event.stopObserving(imgelm1);
	
	hiddenName.style.visibility = visibility;
	
	}

	},

	visibleCarArray : function(carObj){	
		this.visibleCar[this.num] = carObj;
		this.num++;
	},
	
	
	xmlRequest : function(){
	
	var xmlname = 'xml/' + this.viewtype + '.xml';
	
	var showcomp = function(httpObj){
        this.XML = httpObj.responseXML;
        this.selectset().bind(this);
    };
	
	new Ajax.Request(xmlname, {
                method: 'get',
                onFailure: function(httpObj){
                    alert('読み込みエラーが発生しました。ページをリロードしてください。');
                },
                onComplete: showcomp.bind(this)
     });
	},
	
	selectset : function(){
	
	var selectmaterial = this.XML.getElementsByTagName('selectmenu');
	var select_len = selectmaterial.length;
	
	for(var d=0;d<select_len;d++){
		
		var selectbox = new Element('select', { id: this.viewtype + 'select_' + (d+1)});
		
		this.selectdef[d] = new Array();
		var option = ukk.getElementsByTagNameArray('option',selectmaterial[d]);
	
		for(var q=0;q<option.length;q++){
			this.selectdef[d][q] = option[q].firstChild.nodeValue;
			var optionelm = new Option(this.selectdef[d][q],q);
			optionelm.innerHTML=this.selectdef[d][q];
			selectbox.appendChild(optionelm);	
		}
		this.selectnum[d] = 0;
		this.selectTag[d] = selectbox;
		
		new Insertion.Top(this.viewtype + "selectArea_" + (d+1), selectbox);

	}
	
	var checkmaterial = this.XML.getElementsByTagName('optiontxt');

	if(checkmaterial.length >= 1){
	for(var d=0;d<checkmaterial.length;d++){
		
		var dd = parseInt(checkmaterial[d].getAttribute('id'));	
		var p = new Element('p');
		var span = new Element('span', { id: this.viewtype + 'list' + dd});
		var input = new Element('input', { id: this.viewtype + 'ch' + (dd+1), type:'checkbox', disabled: false});
		var label = new Element('label', { "for": input.id});
		label.innerHTML = checkmaterial[dd].getAttribute('value');
		
		span.appendChild(input);
		span.appendChild(label);
		p.appendChild(span);
		
		this.optelems[d] = input;
		this.optListelems[d] = label;
		
		new Insertion.Bottom(this.viewtype + 'optionBox',p);
		
		
		var tooltip = ukk.$('tipbase').cloneNode(true);
		tooltip.style.display ='none';
		tooltip.id = 'tip'+this.viewtype+'list' +dd;
		var tiptxt = ukk.$$('innerTxt',tooltip);
		//var tiptxt = tooltip.getElementsByClassName('innerTxt');

		Event.observe(tooltip,'mouseover', this.tipnone);
		
		tiptxt[0].innerHTML = checkmaterial[d].getAttribute('txt');

		new Insertion.Bottom(ukk.$(thisHash + 'Block'), tooltip)

	}
	
	this.option = this.optelems;
	this.optionList = this.optListelems;
	}
		
	this.selectEventSet();
	
	this.carElm = this.XML.getElementsByTagName('car');
	var cartype1 = this.XML.getElementsByTagName('normal')[0].getElementsByTagName('car');
	var cartype2 = this.XML.getElementsByTagName('kei')[0].getElementsByTagName('car');
	
	var img = new Element('img',{ alt:'new', src:'index_images/new.gif', className:'newpos', width:'32', height:'11'});
	var basediv = new Element('div',{className:'carline clearfix'});
	var cardiv = new Element('div',{className:'carBox'});
	//cardiv.style.position = 'relative';
	
	var q1 = 0;
	var q2 = 0;
	var car1 = cartype1.length;
	var car2 = cartype2.length;
	var timer1;
	var timer2;
	var pre = this;
	var basecp = basediv.cloneNode(true);
	var carimg;
	var doc = document;
	var brtag = /br/g; 

	var carfunc = function(){
		
		if(q1<car1){
		
		carimg = cartype1[q1].getElementsByTagName('carImg')[0].getAttribute('URL');
		var carurl = cartype1[q1].getElementsByTagName('carPage')[0].getAttribute('URL');
		var carname = cartype1[q1].getElementsByTagName('name')[0].firstChild.nodeValue;
			
		
		if(q1%6 == 0){
		basecp = basediv.cloneNode(true);
		if(q1 == 0  && cartype1.length > 6) Element.addClassName(basecp,'clborder');
		new Insertion.Bottom(ukk.$(pre.viewtype + "normalBlock"), basecp);
		//if(cartype1.length <= q){	
		//new Insertion.Bottom(ukk.$("keiBlock"), basecp);
		//}
		}
			
		var carcp = cardiv.cloneNode(true);
						
		carcp.id= thisHash + "car" + q1;
		var p1 = doc.createElement('p');
		p1.className = 'carimg';
		var imgp = new Element('img',{ alt:'', src:carimg, width:'122', height:'65'});


		var blankbox = cartype1[q1].getAttribute('blank');
		
		p1.appendChild(imgp);
		var p2 = doc.createElement('p');
		
		carname = carname.replace(brtag,"<br>");
		
		if(carurl != '#'){
			if(blankbox == 1){
				var a = new Element('a',{href:carurl,target:'_blank'});
			}else{
				var a = new Element('a',{href:carurl});
			}
		}else{
		var a = doc.createElement('span');
		}
		a.innerHTML = carname;
		p2.appendChild(a);
		
		if((q1+1)%6 ==0 && q1 !=0) Element.addClassName(carcp,'right');
		
		carcp.appendChild(p1);
		carcp.appendChild(p2);
		
		//new Insertion.Bottom(carcp,p1);
		//new Insertion.Bottom(carcp,p2);	
		//alert(carcp);
		
		var imgcp = img.cloneNode(false);
		var newbox = cartype1[q1].getAttribute('new');
		if(newbox == 1){
			new Insertion.Top(carcp, imgcp); 		
		}
		
		basecp.appendChild(carcp);
		//new Insertion.Bottom(basecp,carcp);	
	
		q1++;
		pre.loadimg = new Image();
		pre.loadimg.src = carimg;
		pre.loadingimg();
		
		timer1 = setTimeout(carfunc, 10);
		}else{		
		clearTimeout(timer1);
		timer2 = setTimeout(carfunc2, 10);
		return;
		}
		
	}
	timer1 = setTimeout(carfunc, 10);
	
	var carfunc2 = function(){
		if(q2<car2){
		
		carimg = cartype2[q2].getElementsByTagName('carImg')[0].getAttribute('URL');
		var carurl = cartype2[q2].getElementsByTagName('carPage')[0].getAttribute('URL');
		var carname = cartype2[q2].getElementsByTagName('name')[0].firstChild.nodeValue;
		
		
		if(q2%6 == 0){
		basecp = basediv.cloneNode(true);
		if(q2 == 0  && cartype2.length > 6) Element.addClassName(basecp,'clborder');
		new Insertion.Bottom(ukk.$(pre.viewtype + "keiBlock"), basecp);
		}
			
		var carcp = cardiv.cloneNode(true);
		carcp.id=  thisHash + "car" + (q1 +q2);
		var p1 = doc.createElement('p');
		p1.className = 'carimg';
		var imgp = new Element('img',{ alt:'', src:carimg, width:'122', height:'65'});
		
		//var a = document.createElement('a');
		//a.href=carurl;
		//a.appendChild(imgp);
		p1.appendChild(imgp);
		var p2 = doc.createElement('p');

		carname = carname.replace(brtag,"<br>");

		if(carurl != '#'){
		var a = new Element('a',{href:carurl});
		}else{
		var a = doc.createElement('span');
		}
		a.innerHTML = carname;
		p2.appendChild(a);
		
		if((q2+1)%6 ==0 && q2 !=0) Element.addClassName(carcp,'right');
		
		carcp.appendChild(p1);
		carcp.appendChild(p2);
		basecp.appendChild(carcp);
		
		//new Insertion.Bottom(basecp,carcp);	
		
		var imgcp = img.cloneNode(false);
		var newbox = cartype2[q2].getAttribute('new');
		
		//alert(carname+ '--' +pre.carElm[3].getAttribute('new'));
		
		if(newbox == 1){
			new Insertion.Top(carcp, imgcp); 		
		}
		
		pre.loadimg = new Image();
		pre.loadimg.src = carimg;
		pre.loadingimg();
		q2++;
		
		timer2 = setTimeout(carfunc2, 10);
		}else{
		clearTimeout(timer2);
		return;
		}
	}
	
	
	var timeout = function(){
		pre.imgcount = pre.carElm.length;
		clearTimeout(pre.imgtimeout);
		pre.imgcomp();
	}
	if(!safari) this.imgtimeout = setTimeout(timeout, 5000);
								
	if(this.viewtype == 'carlineup'){
		
		var edition = this.XML.getElementsByTagName('edition')[0];
		
		edition = edition.getElementsByTagName('banner');
		var targetdiv = ukk.$('limitedArea');
		var div = new Element('div',{'class':'bnarea clearfix clborder'});
		
		if(edition.length > 0){
		
		for(var u=0;u<edition.length;u++){
			
			var bnimg = new Element('img',{src:edition[u].getElementsByTagName('img')[0].getAttribute('URL'),alt:edition[u].getAttribute('name'),'width':184});
			var bna = new Element('a',{href:edition[u].getElementsByTagName('page')[0].getAttribute('URL')});
			bna.appendChild(bnimg);
			var bnp = new Element('p');

			if(edition[u].getAttribute('blank') == '1')	bna.setAttribute('target','_blank');
			
			if(u > edition.length - 2) Element.removeClassName(div,'clborder');
			
			if((u+1)%2 == 0 && (u+1)%4 != 0 || u%2 == 0 && u%4 != 0) bnp.style.marginRight = '2px';
			if((u+1)%4 == 0) bnp.className = 'right';
			bnp.appendChild(bna);
			div.appendChild(bnp);
			
			new Insertion.Bottom(targetdiv,div);
			if((u+1)%4 == 0) var div = div.cloneNode(false);

		}
		
		}
	}
	
	ret = Element.getHeight(ukk.$(this.viewtype + 'ConditionBlock'));
	
	if(ie) ukk.$(this.viewtype + 'loadArea').style.height = ret + 'px';
	else ukk.$(this.viewtype + 'loadArea').style.minHeight = ret + 'px';

	//new Effect.Morph(this.viewtype + 'Block', {style: 'min-height:'+ret+'px' ,duration:0.8}) 

	},
	
	loadingimg : function(){
          if (this.loadimg.complete) {
                this.imgcomp();
            }else{
                var ref = this;
                if (safari) {
                
                    this.safariOnLoad = function(){
                        if (ref.loadimg.complete) {
                            clearTimeout(timer);
                            ref.imgcomp();
                        }else{
                            var timer = setTimeout(ref.safariOnLoad, 100);
                        };
                    };
                    this.safariOnLoad();
                }else{
                    Event.observe(this.loadimg, 'load', this.imgcomp.bind(this), false);
                }
            }
	
	},
	
	imgcomp : function(){
	this.imgcount++;
	if(this.imgcount >= this.carElm.length){
	clearTimeout(this.imgtimeout);
	ukk.$(this.viewtype + 'carList').style.backgroundImage = 'none';
	ukk.$(this.viewtype + 'ConditionBlock').style.visibility = 'visible';
	//if(ie) ukk.$(this.viewtype + 'loadArea').style.filter = 'alpha(opacity=0)';
	//else ukk.$(this.viewtype + 'loadArea').style.opacity = 0;
	
	boxclassoverFunc(this.viewtype,'carBox');

	}
	}
  
});


function lineupModule(Hash){

thisHash = Hash;

if(!comp[thisHash]) {
comp[thisHash] = true;
	//if(ie) ukk.$(thisHash + 'loadArea').style.filter = 'alpha(opacity=0)';
	//else ukk.$(thisHash + 'loadArea').style.opacity = 0;
if(thisHash == 'accessory') {
	boxclassoverFunc(thisHash,'acceBox');
}else{
	ukk.$(thisHash + 'loadArea').style.visibility = 'hidden';
	Form.reset(thisHash + 'Search');
	imgcount=0;
	var lineup = new carlineupClass(thisHash);
	lineup.xmlRequest();
//lineup.selectEventSet();
}
}

}


//document.observe("dom:loaded", lineupModule);


var boxclassoverFunc = function(viewtype,target){
var __boxheight = 0;
	var txtbox = viewtype + 'Block';
	var boxoverclass = $(txtbox).getElementsByClassName(target);
	//var elems = ukk.$$(target,ukk.$(txtbox))
	var elems = $A(boxoverclass);
	elems.each(function(obj){
		
		if(target == "acceBox"){
			
		obj.observe('click',boxclick2);
		
		}else{
			
		var imgobj = obj.getElementsByTagName('img');
		
		if(imgobj.length > 1) var imgobj1 = imgobj[1];
		else var imgobj1 = imgobj[0];
		if(obj.getElementsByTagName("a")[0]){
		Event.observe(imgobj1,'mouseover',imgovert);
		Event.observe(imgobj1,'mouseout',imgoverf);
		Event.observe(imgobj1,'click',boxclick);
		}
		}
		
		if(obj.getElementsByTagName("a")[0]){
		obj.observe('mouseover',boxovert);
		obj.observe('mouseout',boxoverf);
		}
		if(__boxheight < Element.getHeight(obj)){
		__boxheight = Element.getHeight(obj);
		}

	});
	
	elems.each(function(obj){
		if(ie){
		obj.style.height = __boxheight;
		}else{
		obj.style.minHeight = __boxheight;
		}
	});

	/*if(sv != 2){
	new Effect.Opacity(ukk.$(viewtype + 'loadArea'), { from:0.0, to:1.0,duration :0.3,
	beforeStartInternal: function(effect) {
       ukk.$(viewtype + 'loadArea').style.visibility = 'visible';
	   
    }
	});
	}else{*/
		ukk.$(viewtype + 'loadArea').style.visibility = 'visible';
	//	ukk.$(viewtype + 'loadArea').style.opacity = 1;
	//}
	
}
var boxclick = function(e){
	//Element.removeClassName(this,'boxovero');
	var imgobj = this.parentNode.nextSibling;
	if(!imgobj.getElementsByTagName("a")[0]) return;
	var boxhref = imgobj.getElementsByTagName("a")[0];
	
	if(boxhref.target =="_blank"){
	window.open(boxhref.href);
	}else{
	location.href = boxhref.href;
	}
}
var boxclick2 = function(e){
	var boxhref = this.getElementsByTagName("a")[0];
	if(boxhref.target =="_blank"){
	window.open(boxhref.href);
	}else{
	location.href = boxhref.href;
	}
	location.href = boxhref.href;
	if (e.target) {
	e.preventDefault(); 
	}else if (window.event.srcElement) { 
	window.event.returnValue=false;
   	}
}
var imgovert = function(){
		this.style.cursor = "pointer";
}
var imgoverf = function(){
		this.style.cursor = "auto";
}
var boxovert = function(event){
		//event.stop();
		Element.addClassName(this,'boxovero');

		if(thisHash == 'accessory') this.style.cursor = "pointer";
		//ukk.$('tipbase').style.display = 'none';
}
var boxoverf = function(){
		Element.removeClassName(this,'boxovero');
		//new Effect.Morph(this, {style:'border: 1px solid #ffffff;',duration:0.3}) 

		if(thisHash == 'accessory') this.style.cursor = "auto";
}


/* tab */

var stayflag = new Array();

var nowStayNum;
var prevItem;
var prevElem;
var tabElem;
var thisHash = location.hash;

var hashflg = true;
var locationHref = location.href;
thislocation = locationHref.split('#');

var lineupTabover = function (e){
	

	document.body.scrollTop = 0;

	var index = ukk.$('carlineupBlock');
	var index2 = ukk.$('bizcarBlock');
	var index3 = ukk.$('heartyBlock');
	var index4 = ukk.$('accessoryBlock');
	
	if(ukk.$("lineupTab")){
		var tabb = ukk.$('lineupTab').getElementsByTagName('a');
		var imgtag = ukk.$("lineupTab").getElementsByTagName('img');
	}else{
		return;
	}
	
	

	
	var fieldimgnum = imgtag.length;
	var prefimg = [];
	
	
	for(var z=0; z<fieldimgnum;z++){
				var fimg = imgtag[z].src; 
				
				prefimg[z] = new Image();
    			prefimg[z].src = preimgfunc(fimg);
				
				var tbbhref = tabb[z].href.split('#'); 
				var tabid = ukk.$(tbbhref[1] + 'Block')
							
				stayflag[z] = true;
				var imgoverObj = new lineupTaboverfunc(imgtag[z],prefimg[z],fimg,z,tabid);

				imgoverObj.evset();
				
				//stayflag[z] = false;
	}
	
	
	
	
	index.style.display ="none";
	index2.style.display ="none";
	index3.style.display ="none";
	index4.style.display ="none";
	
	if(thisHash == "#bizcar"){
		index2.style.display ="block";
		tabElem = index2;
		nowStayNum = 1;
	}else if(thisHash == "#hearty"){
		index3.style.display ="block";
		tabElem = index3;
		nowStayNum = 2;
	}else if(thisHash == "#accessory"){
		index4.style.display ="block";
		tabElem = index4;
		nowStayNum = 3;
	}else{
		index.style.display ="block";
		tabElem = index;
		nowStayNum = 0;
		thisHash = "#carlineup";
	}
	
	thisHash = thisHash.slice(1,thisHash.length);
	prevElem = imgtag[nowStayNum];
	prevItem = imgtag[nowStayNum].src;
	imgtag[nowStayNum].src= preimgfunc(imgtag[nowStayNum].src);
	stayflag[nowStayNum] = false;
	
	lineupModule(thisHash);

}

var lineupTaboverfunc = Class.create({
	
	initialize : function(imgelm,oversrc,thissrc,itemNum,tabid){
		this.nowelm = imgelm;
		this.oversrc = oversrc;
		this.basesrc = thissrc;
		this.itemNum = itemNum;
		this.tabid = tabid
	},
	
	evset : function(){

		var funcoverout = (function(lineupTaboverfunc){

		return function(e){

		if(e.type == 'mouseover' && stayflag[lineupTaboverfunc.itemNum]){
			this.src = lineupTaboverfunc.oversrc.src;
		}else if(e.type == 'mouseout' && stayflag[lineupTaboverfunc.itemNum]){
			this.src = lineupTaboverfunc.basesrc;
		}else if(e.type == 'click' && stayflag[lineupTaboverfunc.itemNum]){
			
			this.src = lineupTaboverfunc.basesrc;						
			stopbubble(e);
			stayflag[nowStayNum] = true;
			if(prevItem) prevElem.src = prevItem;
			if(tabElem) tabElem.style.display = 'none';
		stayflag[lineupTaboverfunc.itemNum] = false;
				
			this.src = lineupTaboverfunc.oversrc.src;
		lineupTaboverfunc.tabid.style.display = 'block';			

			nowStayNum = lineupTaboverfunc.itemNum;
			prevItem = lineupTaboverfunc.basesrc;
			prevElem = lineupTaboverfunc.nowelm;
			tabElem = lineupTaboverfunc.tabid;
			
			
			var mhash = lineupTaboverfunc.tabid.id.replace('Block','');
			location.hash = mhash;
			
			lineupModule(mhash);
			
			
		}else{
			stopbubble(e);
			return false;	
		}
		}
	
		})(this);

		Event.observe(this.nowelm,'mouseover', funcoverout);
		Event.observe(this.nowelm,'mouseout', funcoverout);
		Event.observe(this.nowelm,'click', funcoverout);
	}


});

DOMready.tgFunc(lineupTabover);

//document.observe("dom:loaded", lineupTabover);


//document.observe("dom:loaded", test2);


function lineupgnv(){
	
	if(ukk.$("float1")){
		var gnvlink = ukk.$("float1").getElementsByTagName('a');
	}
	
	
	var gnvfunc = function(){
	location.href = this.href;
	location.reload();
	}
	if(ukk.$("float1")){
	for(z=0; z<gnvlink.length;z++){
		Event.observe(gnvlink[z],'click', gnvfunc);
	}
	}
	
}

DOMready.tgFunc(lineupgnv);
